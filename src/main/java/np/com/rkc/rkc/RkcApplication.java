package np.com.rkc.rkc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RkcApplication {

	public static void main(String[] args) {
		SpringApplication.run(RkcApplication.class, args);
	}

}
